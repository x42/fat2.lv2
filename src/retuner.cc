// -----------------------------------------------------------------------
//
//  Copyright (C) 2009-2011 Fons Adriaensen <fons@linuxaudio.org>
//  Copyright (C) 2016-2021 Robin Gareus <robin@gareus.org>
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// -----------------------------------------------------------------------

#include "retuner.h"

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace LV2AT2;

Retuner::Retuner (int fsamp)
	: _fsamp (fsamp)
	, _corrfilt (1.0f)
	, _corrgain (1.0f)
	, _corroffs (0.0f)
	, _manratio (0.0f)
	, _pitch_inp (fsamp)
	, _pitch_key (fsamp)
{
	if (_fsamp < 64000) {
		/* At 44.1 and 48 kHz resample to double rate. */
		_upsamp = true;
		_ipsize = 4096;
		_frsize = 128;
		_resampler.setup (1, 2, 1, 32); // 32 is medium quality.
		/* Prefeed some input samples to remove delay. */
		_resampler.inp_count = _resampler.filtlen () - 1;
		_resampler.inp_data  = 0;
		_resampler.out_count = 0;
		_resampler.out_data  = 0;
		_resampler.process ();
	} else if (_fsamp < 128000) {
		/* 88.2 or 96 kHz. */
		_upsamp = false;
		_ipsize = 4096;
		_frsize = 256;
	} else {
		/* 192 kHz, double time domain buffers sizes. */
		_upsamp = false;
		_ipsize = 8192;
		_frsize = 512;
	}

	_ipbuff = new float[_ipsize + 3]; // Resampled or filtered input
	_keybuf = new float[_ipsize];     // Resampled or filtered key
	_xffunc = new float[_frsize];     // Crossfade function

	/* Clear input buffers. */
	memset (_ipbuff, 0, (_ipsize + 1) * sizeof (float));
	memset (_keybuf, 0, _ipsize * sizeof (float));

	/* Create crossfade function, half of raised cosine. */
	for (int i = 0; i < _frsize; i++) {
		_xffunc[i] = 0.5 * (1 - cosf (M_PI * i / _frsize));
	}

	/* Initialise all counters and other state. */
	_count   = 0;
	_cycle   = _frsize;
	_error   = 0.0f;
	_ratio   = 1.0f;
	_xfade   = false;
	_ipindex = 0;
	_kyindex = 0;
	_frindex = 0;
	_frcount = 0;
	_rindex1 = _ipsize / 2;
	_rindex2 = 0;
}

Retuner::~Retuner (void)
{
	delete[] _ipbuff;
	delete[] _keybuf;
	delete[] _xffunc;
}

int
Retuner::process (int nfram, float const* inp, float const* key, float* out)
{
	float u1, u2, v;

	/* Pitch shifting is done by resampling the input at the
	 * required ratio, and eventually jumping forward or back
	 * by one or more pitch period(s). Processing is done in
	 * fragments of '_frsize' frames, and the decision to jump
	 * forward or back is taken at the start of each fragment.
	 * If a jump happens we crossfade over one fragment size.
	 * Every 4 fragments a new pitch estimate is made. Since
	 * _fftsize = 16 * _frsize, the estimation window moves
	 * by 1/4 of the FFT length.
	 */

	int   fi = _frindex; // Write index in current fragment.
	float r1 = _rindex1; // Read index for current input frame.
	float r2 = _rindex2; // Second read index while crossfading.

	/* No assumptions are made about fragments being aligned
	 * with process() calls, so we may be in the middle of
	 * a fragment here.
	 */

	while (nfram) {
		/* Don't go past the end of the current fragment. */
		int k = _frsize - fi;
		if (nfram < k) {
			k = nfram;
		}
		nfram -= k;

		if (_upsamp) {
			/* At 44.1 and 48 kHz upsample by 2. */
			_resampler.inp_count = k;
			_resampler.inp_data  = inp;
			_resampler.out_count = 2 * k;
			_resampler.out_data  = _ipbuff + _ipindex;
			_resampler.process ();
			_ipindex += 2 * k;
		} else {
			/* At higher sample rates apply lowpass filter.
			 * XXX Not implemented yet, just copy.
			 */
			memcpy (_ipbuff + _ipindex, inp, k * sizeof (float));
			_ipindex += k;
		}

		/* Extra samples for interpolation. */
		_ipbuff[_ipsize + 0] = _ipbuff[0];
		_ipbuff[_ipsize + 1] = _ipbuff[1];
		_ipbuff[_ipsize + 2] = _ipbuff[2];

		inp += k;
		if (_ipindex == _ipsize) {
			_ipindex = 0;
		}

		/* copy key input */
		memcpy (_keybuf + _kyindex, key, k * sizeof (float));
		key += k;
		_kyindex += k;
		if (_kyindex == _ipsize) {
			_kyindex = 0;
		}

		/* Process available samples. */
		float dr = _upsamp ? 2.f * _ratio : _ratio;

		if (_xfade) {
			/* Interpolate and crossfade. */
			while (k--) {
				int i;
				i  = (int)r1;
				u1 = cubic (_ipbuff + i, r1 - i);

				i  = (int)r2;
				u2 = cubic (_ipbuff + i, r2 - i);

				v      = _xffunc[fi++];
				*out++ = (1 - v) * u1 + v * u2;

				r1 += dr;
				if (r1 >= _ipsize)
					r1 -= _ipsize;
				r2 += dr;
				if (r2 >= _ipsize)
					r2 -= _ipsize;
			}
		} else {
			/* Interpolation only. */
			fi += k;
			while (k--) {
				int i  = (int)r1;
				*out++ = cubic (_ipbuff + i, r1 - i);
				r1 += dr;
				if (r1 >= _ipsize)
					r1 -= _ipsize;
			}
		}

		/* If at end of fragment check for jump. */
		if (fi == _frsize) {
			fi = 0;

			/* Estimate the pitch every 4th fragment. */
			if (++_frcount == 4) {
				_frcount = 0;

				_cycle   = _pitch_inp.findcycle (_ipbuff, _ipindex, _ipsize, _upsamp ? 2 : 1);
				float ck = _pitch_key.findcycle (_keybuf, _kyindex, _ipsize, 1);

				if (ck) {
					_cycle_key = ck;
					_count_key = 0;
				} else if (++_count_key > 5) {
					_cycle_key = 0;
					_count_key = 5;
				}

				if (_cycle) {
					/* If the pitch estimate succeeds, calculate the ratio */
					_count = 0;
					if (_cycle_key) {
						/* f_key = _fsamp / _cycle_key [Hz]
						 * f_Inp = _fsamp / _cycle [Hz]
						 */
						_error = log2f (_cycle_key / _cycle);
						// TODO if the difference is small, filter:
						//_error += _corrfilt * (dm - _error);
						//printf ("PITCH INPUT %f, Key: %f Ratio: %f ST: %f\n", _fsamp / _cycle, _fsamp / _cycle_key, _cycle / _cycle_key, _error);
						if (_cycle / _cycle_key < 0.5 || _cycle / _cycle_key > 2.0) {
							_error = 0;
						}
					}
				} else if (++_count > 5) {
					/* If the pitch estimate fails, the current
					 * ratio is kept for 5 fragments. After that
					 * the signal is considered unvoiced and the
					 * pitch error is reset.
					 */
					_count = 5;
					_cycle = _frsize;
					_error = 0;
				}

				if (_manratio >= 0.5 && _manratio <= 2.0) {
					_ratio = _manratio * _corrgain * powf (2.0f, _corroffs / 12.0f);
				} else {
					_ratio = powf (2.0f, _corroffs / 12.0f - _error * _corrgain);
				}
			}

			// If the previous fragment was crossfading,
			// the end of the new fragment that was faded
			// in becomes the current read position.
			if (_xfade) {
				r1 = r2;
			}

			/* A jump must correspond to an integer number
			 * of pitch periods, and to avoid reading outside
			 * the circular input buffer limits it must be at
			 * least one fragment size.
			 */
			float dr = _cycle * (int)(ceilf (_frsize / _cycle));
			float dp = dr / _frsize;
			float ph = r1 - _ipindex;
			if (ph < 0) {
				ph += _ipsize;
			}
			if (_upsamp) {
				ph /= 2;
				dr *= 2;
			}
			ph = ph / _frsize + 2 * _ratio - 10;
			if (ph > 0.5f) {
				/* Jump back by 'dr' frames and crossfade. */
				_xfade = true;
				r2     = r1 - dr;
				if (r2 < 0)
					r2 += _ipsize;
			} else if (ph + dp < 0.5f) {
				/* Jump forward by 'dr' frames and crossfade. */
				_xfade = true;
				r2     = r1 + dr;
				if (r2 >= _ipsize)
					r2 -= _ipsize;
			} else
				_xfade = false;
		}
	}

	/* Save local state. */
	_frindex = fi;
	_rindex1 = r1;
	_rindex2 = r2;

	return 0;
}

float
Retuner::cubic (float* v, float a)
{
	float b, c;

	b = 1 - a;
	c = a * b;
	return (1.0f + 1.5f * c) * (v[1] * b + v[2] * a) - 0.5f * c * (v[0] * b + v[1] + v[2] + v[3] * a);
}

Retuner::AutoCorr::AutoCorr (int rate)
	: _fsamp (rate)
{
	if (_fsamp < 64000) {
		/* At 44.1 and 48 kHz */
		_fftlen = 2048;
	} else if (_fsamp < 128000) {
		/* 88.2 or 96 kHz. */
		_fftlen = 4096;
	} else {
		/* 192 kHz, double time domain buffers sizes. */
		_fftlen = 8192;
	}

	/* Accepted correlation peak range, corresponding to 60..1200 Hz. */
	_ifmin = rate / 1200;
	_ifmax = rate / 60;

	_fftTwind = (float*)fftwf_malloc (_fftlen * sizeof (float)); // Window function
	_fftWcorr = (float*)fftwf_malloc (_fftlen * sizeof (float)); // Autocorrelation of window
	_fftTdata = (float*)fftwf_malloc (_fftlen * sizeof (float)); // Time domain data for FFT
	_fftFdata = (fftwf_complex*)fftwf_malloc ((_fftlen / 2 + 1) * sizeof (fftwf_complex));

	/* FFTW3 plans */
	_fwdplan = fftwf_plan_dft_r2c_1d (_fftlen, _fftTdata, _fftFdata, FFTW_ESTIMATE);
	_invplan = fftwf_plan_dft_c2r_1d (_fftlen, _fftFdata, _fftTdata, FFTW_ESTIMATE);

	/* Create window, raised cosine. */
	for (int i = 0; i < _fftlen; i++) {
		_fftTwind[i] = 0.5 * (1 - cosf (2 * M_PI * i / _fftlen));
	}

	/* Compute window autocorrelation and normalise it. */
	fftwf_execute_dft_r2c (_fwdplan, _fftTwind, _fftFdata);

	int h = _fftlen / 2;
	for (int i = 0; i < h / 2; i++) {
		float x         = _fftFdata[i][0];
		float y         = _fftFdata[i][1];
		_fftFdata[i][0] = x * x + y * y;
		_fftFdata[i][1] = 0;
	}

	_fftFdata[h][0] = 0;
	_fftFdata[h][1] = 0;

	fftwf_execute_dft_c2r (_invplan, _fftFdata, _fftWcorr);

	const float t = _fftWcorr[0];
	for (int i = 0; i < _fftlen; i++) {
		_fftWcorr[i] /= t;
	}
}

Retuner::AutoCorr::~AutoCorr ()
{
	fftwf_free (_fftTwind);
	fftwf_free (_fftWcorr);
	fftwf_free (_fftTdata);
	fftwf_free (_fftFdata);
	fftwf_destroy_plan (_fwdplan);
	fftwf_destroy_plan (_invplan);
}

float
Retuner::AutoCorr::findcycle (float const* in, size_t off, size_t size, size_t stride)
{
	assert ((size & (size - 1)) == 0);

	for (int i = 0; i < _fftlen; ++i) {
		int p        = off & (size - 1);
		_fftTdata[i] = _fftTwind[i] * in[p];
		off += stride;
	}

	fftwf_execute_dft_r2c (_fwdplan, _fftTdata, _fftFdata);

	const float f = _fsamp / (_fftlen * 3e3f);
	const int   h = _fftlen / 2;

	for (int i = 0; i < h; i++) {
		const float x = _fftFdata[i][0];
		const float y = _fftFdata[i][1];
		const float m = i * f;

		_fftFdata[i][0] = (x * x + y * y) / (1 + m * m);
		_fftFdata[i][1] = 0;
	}

	_fftFdata[h][0] = 0;
	_fftFdata[h][1] = 0;

	fftwf_execute_dft_c2r (_invplan, _fftFdata, _fftTdata);

	const float t = _fftTdata[0] + 0.1f;
	for (int i = 0; i < h; i++) {
		_fftTdata[i] /= (t * _fftWcorr[i]);
	}

	int   i;
	float pk = _fftTdata[0];
	for (i = 4; i < _ifmax; i += 4) {
		const float y = _fftTdata[i];
		if (y > pk) {
			break;
		}
		pk = y;
	}

	i -= 4;

	_cycle = 0;

	if (i >= _ifmax) {
		return _cycle;
	}
	if (i < _ifmin) {
		i = _ifmin;
	}

	float x = _fftTdata[--i];
	float y = _fftTdata[++i];
	float m = 0;
	int   j = 0;

	while (i <= _ifmax) {
		const float t = y * _fftWcorr[i];
		const float z = _fftTdata[++i];
		if ((t > m) && (y >= x) && (y >= z) && (y > 0.8f)) {
			j = i - 1;
			m = t;
		}
		x = y;
		y = z;
	}

	if (j) {
		const float x = _fftTdata[j - 1];
		const float y = _fftTdata[j];
		const float z = _fftTdata[j + 1];
		if (fabsf (z - 2 * y + x) > 2e-9f) {
			_cycle = j + 0.5f * (x - z) / (z - 2 * y + x - 1e-9f);
		}
	}
	return _cycle;
}
