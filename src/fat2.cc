/* fat2 -- LV2 auto-re-tuner
 *
 * Copyright (C) 2016-2021 Robin Gareus <robin@gareus.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <lv2/lv2plug.in/ns/ext/log/logger.h>
#include <lv2/lv2plug.in/ns/lv2core/lv2.h>

#include "fat2.h"
#include "retuner.h"

static pthread_mutex_t fftw_planner_lock = PTHREAD_MUTEX_INITIALIZER;
static unsigned int    instance_count    = 0;

typedef struct {
	/* I/O Ports */
	float* port[FAT_LAST];

	/* internal state */
	LV2AT2::Retuner* retuner;

	float latency;
} Fat2;

/* *****************************************************************************
 * LV2 Plugin
 */

static LV2_Handle
instantiate (const LV2_Descriptor*     descriptor,
             double                    rate,
             const char*               bundle_path,
             const LV2_Feature* const* features)
{
	Fat2* self = (Fat2*)calloc (1, sizeof (Fat2));

	if (0 != strcmp (descriptor->URI, FAT2_URI)) {
		free (self);
		return 0;
	}

	pthread_mutex_lock (&fftw_planner_lock);
	self->retuner = new LV2AT2::Retuner (rate);
	++instance_count;
	pthread_mutex_unlock (&fftw_planner_lock);

	// compare Retuner c'tor
	if (rate < 64000) {
		self->latency = 1024;
	} else if (rate < 128000) {
		self->latency = 2048;
	} else {
		self->latency = 4096;
	}
	return (LV2_Handle)self;
}

static void
connect_port (LV2_Handle instance,
              uint32_t   port,
              void*      data)
{
	Fat2* self = (Fat2*)instance;
	if (port < FAT_LAST) {
		self->port[port] = (float*)data;
	}
}

static void
run (LV2_Handle instance, uint32_t n_samples)
{
	Fat2* self = (Fat2*)instance;

	*self->port[FAT_LTNC] = self->latency;

	if (n_samples == 0) {
		return;
	}

	/* push config to retuner */
	self->retuner->set_corrfilt (*self->port[FAT_FILT]);
	self->retuner->set_corrgain (*self->port[FAT_CORR]);
	self->retuner->set_corroffs (*self->port[FAT_OFFS]);
	self->retuner->set_manratio (*self->port[FAT_RAIO]);

	/* process */
	self->retuner->process (n_samples, self->port[FAT_INPUT], self->port[FAT_KEY], self->port[FAT_OUTPUT]);

	/* report */
	*self->port[FAT_ERRR] = self->retuner->get_error ();
}

static void
cleanup (LV2_Handle instance)
{
	Fat2* self = (Fat2*)instance;
	pthread_mutex_lock (&fftw_planner_lock);
	delete self->retuner;
	if (instance_count > 0) {
		--instance_count;
	}
#ifdef WITH_STATIC_FFTW_CLEANUP
	/* use this only when statically linking to a local fftw!
	 *
	 * "After calling fftw_cleanup, all existing plans become undefined,
	 *  and you should not attempt to execute them nor to destroy them."
	 * [http://www.fftw.org/fftw3_doc/Using-Plans.html]
	 *
	 * If libfftwf is shared with other plugins or the host this can
	 * cause undefined behavior.
	 */
	if (instance_count == 0) {
		fftwf_cleanup ();
	}
#endif
	pthread_mutex_unlock (&fftw_planner_lock);
	free (instance);
}

const void*
extension_data (const char* uri)
{
	return NULL;
}

static const LV2_Descriptor descriptor = {
	FAT2_URI,
	instantiate,
	connect_port,
	NULL,
	run,
	NULL,
	cleanup,
	extension_data
};

#undef LV2_SYMBOL_EXPORT
#ifdef _WIN32
#  define LV2_SYMBOL_EXPORT __declspec(dllexport)
#else
#  define LV2_SYMBOL_EXPORT __attribute__ ((visibility ("default")))
#endif
LV2_SYMBOL_EXPORT
const LV2_Descriptor*
lv2_descriptor (uint32_t index)
{
	switch (index) {
		case 0:
			return &descriptor;
		default:
			return NULL;
	}
}
