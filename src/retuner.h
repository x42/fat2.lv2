// -----------------------------------------------------------------------
//
//  Copyright (C) 2009-2011 Fons Adriaensen <fons@linuxaudio.org>
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// -----------------------------------------------------------------------

#ifndef __RETUNER_H
#define __RETUNER_H

#include "resampler.h"
#include <fftw3.h>

namespace LV2AT2 {

class Retuner
{
public:
	Retuner (int fsamp);
	~Retuner (void);

	int process (int nfram, float const* inp, float const* key, float* out);

	void
	set_corrfilt (float v)
	{
		_corrfilt = v;
	}

	void
	set_corrgain (float v)
	{
		_corrgain = v;
	}

	void
	set_corroffs (float v)
	{
		_corroffs = v;
	}

	void
	set_manratio (float v)
	{
		_manratio = v;
	}


	float
	get_error (void)
	{
		return 12.0f * _error;
	}

private:
	class AutoCorr
	{
	public:
		AutoCorr (int rate);
		~AutoCorr ();
		float findcycle (float const*, size_t, size_t, size_t);

	private:
		int   _fsamp;
		int   _fftlen;
		int   _ifmin;
		int   _ifmax;
		float _cycle; // XXX

		float*         _fftTwind;
		float*         _fftWcorr;
		float*         _fftTdata;
		fftwf_complex* _fftFdata;
		fftwf_plan     _fwdplan;
		fftwf_plan     _invplan;
	};

	static float cubic (float* v, float a);

	/* Config */
	int  _fsamp;
	bool _upsamp;
	int  _ipsize;
	int  _frsize;

	/* Params */
	float _corrfilt;
	float _corrgain;
	float _corroffs;
	float _manratio;

	int   _count;
	float _cycle;
	int   _count_key;
	float _cycle_key;
	float _error;
	float _ratio;

	/* buffer indices */
	int   _ipindex;
	int   _kyindex;
	bool  _xfade;
	int   _frindex;
	int   _frcount;
	float _rindex1;
	float _rindex2;

	float* _ipbuff;
	float* _xffunc;
	float* _keybuf;

	LV2AT2::Resampler _resampler;
	AutoCorr          _pitch_inp;
	AutoCorr          _pitch_key;
};

}; // namespace LV2AT2

#endif
