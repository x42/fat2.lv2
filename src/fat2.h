#define FAT2_URI "http://gareus.org/oss/lv2/fat2"

typedef enum {
	FAT_INPUT = 0,
	FAT_KEY,
	FAT_OUTPUT,

	FAT_FILT,
	FAT_CORR,
	FAT_OFFS,
	FAT_RAIO,

	FAT_ERRR,
	FAT_LTNC,
	FAT_LAST
} PortIndex;
