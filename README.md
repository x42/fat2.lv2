fat2.lv2 - AutoTune-2
=====================

fat2.lv2 is an auto-tuner based on Fons Adriaensen's zita-at1.

Install
-------

Compiling fat2.lv2 requires the LV2 SDK, fftw, gnu-make, and a c-compiler.

```bash
git clone https://gitlab.com/x42/fat2.lv2.git
cd fat2.lv2
make

ln -s `pwd`/build ~/.lv2/fat2.lv2
#sudo make install PREFIX=/usr
```
